# Use NodeJS to implement web server.
## Requirements:
• Use standard http module to implement simple web-server;

• Use fs module to create/modify files in file system;

• Organize all logs inside one JSON file;

• Write every request info to array in json file.

## Acceptance criteria:
• Server saves file on POST /file request and responds with 200 status, use
‘filename' and ‘content’ url query params to transfer file data.

• Server returns file content on GET /file/:filename request, use 'filename' url
parameter to determine what file client wants to retrieve.

• In case there are no files with such name found, return 400 status.

• Server responds to GET /logs request with all saved logs as JSON.

• Every request information can be found in json file on server side;

## Optional criteria:
• Server handles errors and validates input params for POST /file request.

• Ability to get logs for specified date range( from and to url query params as
timestamps);

## Notes:
1) All information about request can be found in request object

2) Json file should look like:

{
«logs»: [
	 	 {message: ‘New file with name ‘book.txt’ saved’, time: 97346772843}
]
}

Use same structure for GET /logs response