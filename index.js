const http = require('http');
const fs = require('fs');
const url = require('url');
const util = require('util');

const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const stat = util.promisify(fs.stat);
const mkdir = util.promisify(fs.mkdir);

const LOGS_FILE_PATH = './logs.json';
const FILE_DIR_PATH = '/file';
const PORT = +process.env.PORT || 8080;
const STATUS_CODES = {
  ok: 200,
  badRequest: 400,
  notFound: 404,
};

const logger = async message => {
  const logItem = { message, time: Date.now() };
  try {
    const log = await readFile(LOGS_FILE_PATH);
    const newLog = JSON.parse(log);
    newLog.logs.push(logItem);
    await writeFile(LOGS_FILE_PATH, JSON.stringify(newLog));
  } catch (err) {
    if (err.code === 'ENOENT') {
      await writeFile(LOGS_FILE_PATH, JSON.stringify({ logs: [logItem] }));
    }
  }
};

module.exports = async () => {
  try {
    await stat('./file');
  } catch (err) {
    if (err.code === 'ENOENT') {
      mkdir('./file').catch(err => console.log(err));
    }
  }

  const server = http.createServer(async (req, res) => {
    const { pathname, query } = url.parse(req.url, true);
    const resProps = {
      type: 'text/html',
    };

    switch (req.method) {
      case 'POST':
        if (pathname === FILE_DIR_PATH) {
          if (query.filename && query.content) {
            try {
              await writeFile(
                `.${FILE_DIR_PATH}/${query.filename}`,
                `${query.content}`
              );
              resProps.log = `New file with name ${query.filename} saved`;
              resProps.status = STATUS_CODES.ok;
            } catch (error) {
              console.log(error);
            }
          } else {
            resProps.log = `Incorrect params`;
            resProps.status = STATUS_CODES.badRequest;
          }
        } else {
          resProps.log = `POST request failed - incorrect path`;
          resProps.status = STATUS_CODES.badRequest;
        }
        break;
      case 'GET':
        console.log(pathname);
        if (pathname === '/logs') {
          try {
            const data = await readFile(LOGS_FILE_PATH, 'utf8');
            const log = JSON.parse(data);
            const filteredLog = {};

            if (query.from && query.to) {
              filteredLog.logs = log.logs.filter(
                item => item.time >= query.from && item.time <= query.to
              );
              resProps.end = JSON.stringify(filteredLog);
            } else {
              resProps.end = JSON.stringify(log);
            }
            resProps.log = 'Logs opened';
            resProps.status = STATUS_CODES.ok;
            resProps.type = 'aplication/json';
          } catch (error) {
            resProps.log = 'Log file not found';
            resProps.status = STATUS_CODES.notFound;
          }
        } else if (pathname.slice(1, 5) === 'file') {
          const filename = pathname.slice(6);
          try {
            resProps.end = await readFile(`./file/${filename}`, 'utf8');
            resProps.status = STATUS_CODES.ok;
          } catch (error) {
            resProps.log = 'File not found';
            resProps.status = STATUS_CODES.badRequest;
          }
        } else {
          resProps.log = 'GET request - incorrect pathname';
          resProps.status = STATUS_CODES.badRequest;
        }
        break;
      default:
        resProps.log = 'Incorrect request type';
        resProps.status = STATUS_CODES.badRequest;
    }
    console.log(resProps.status);
    logger(resProps.log);
    res.writeHead(resProps.status, { 'Content-type': resProps.type });
    res.end(resProps.end || resProps.log);
  });
  server.listen(PORT);
};
